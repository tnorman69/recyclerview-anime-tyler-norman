package com.alecbrando.homeworkrecyclerview.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.alecbrando.homeworkrecyclerview.databinding.FragmentListBinding
import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.model.models.Data
import com.alecbrando.homeworkrecyclerview.util.Resource
import com.alecbrando.homeworkrecyclerview.util.getJsonDataFromAsset
import com.alecbrando.homeworkrecyclerview.view.adapter.AnimeAdapter
import com.alecbrando.homeworkrecyclerview.viewmodels.MainViewModel
import com.google.gson.Gson

class ListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!

    private val mainViewModel by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = getJsonDataFromAsset(requireContext(), "anime.json")
        val actualData = Gson().fromJson(data, Animes::class.java)
        mainViewModel.getAnimes(actualData)
        setObservers()
    }

    private fun setObservers(){
        mainViewModel.animesList.observe(viewLifecycleOwner) {
            when(it){
                is Resource.Loading -> {
                    binding.progressCircular.show()
                }
                is Resource.Success -> {
                    binding.progressCircular.hide()
                    binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
                    binding.recyclerView.adapter = AnimeAdapter(::itemClicked).apply {
                        giveData(it.data)
                    }
                }

            }
        }
    }

    fun itemClicked(data: Data) {
        val action = ListFragmentDirections.actionListFragmentToDetailFragment(data)
        findNavController().navigate(action)
    }

}