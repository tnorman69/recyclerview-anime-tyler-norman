package com.alecbrando.homeworkrecyclerview.model.models

data class LargeX(
    val height: Int,
    val width: Int
)