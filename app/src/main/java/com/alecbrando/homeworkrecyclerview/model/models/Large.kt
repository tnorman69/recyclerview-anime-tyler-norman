package com.alecbrando.homeworkrecyclerview.model.models

data class Large(
    val height: Int,
    val width: Int
)